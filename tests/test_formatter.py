import time
from typing import Any

import pytest
from pytest_mock import MockerFixture
from logger import LogRecord
from logger.formatter import Formatter, PercentStyle


@pytest.fixture()
def percent_style() -> PercentStyle:
    return PercentStyle(None)


@pytest.fixture()
def error() -> Exception:
    try:
        raise ValueError()
    except Exception as e:
        ex = e
    return ex


def test_percent_style_formatter_default_uses_time(percent_style: PercentStyle) -> None:
    assert not percent_style.is_uses_time()


def test_percent_style_formatter_default_format(
    percent_style: PercentStyle, simple_log_record: LogRecord
) -> None:
    percent_style._fmt = "%(msg)s"
    formatted = percent_style.format(simple_log_record)
    assert formatted == simple_log_record.msg


def test_percent_style_formatter_custom_format_valid(
    simple_log_record: LogRecord,
) -> None:
    custom_format = "%(filename)s %(name)s %(msg)s"
    style = PercentStyle(custom_format)
    formatted = style.format(simple_log_record)
    assert formatted == custom_format % dict(
        filename=simple_log_record.filename,
        name=simple_log_record.name,
        msg=simple_log_record.msg,
    )


def test_percent_style_formatter_custom_format_invalid() -> None:
    with pytest.raises(ValueError):
        custom_format = "%(message)"
        PercentStyle(custom_format)


def test_percent_style_formatter_custom_format_time() -> None:
    custom_format = "%(asctime)s %(message)s"
    style = PercentStyle(custom_format)
    assert style.is_uses_time()


def test_percent_style_formatter_custom_format_unknown_field(
    simple_log_record: LogRecord,
) -> None:
    custom_format = "%(unknown)s"
    style = PercentStyle(custom_format)
    with pytest.raises(ValueError):
        assert style.format(simple_log_record)


def test_percent_style_formatter_custom_format_defaults(
    simple_log_record: LogRecord,
) -> None:
    default_value = "default"
    custom_format = "%(default_field)s"
    style = PercentStyle(custom_format, defaults=dict(default_field=default_value))
    assert style.format(simple_log_record) == default_value


def test_formatter_format_time() -> None:
    create_time = 1637400026
    lr = LogRecord(
        name="simple",
        level="info",
        msg="simple message",
        create_time=create_time,
    )
    formatter = Formatter()
    assert formatter.format_time(lr) == "2021-11-20 09:20:26"


def test_formatter_format_time_custom() -> None:
    create_time = 1637400026
    lr = LogRecord(
        name="simple",
        level="info",
        msg="simple message",
        create_time=create_time,
    )
    formatter = Formatter()
    assert formatter.format_time(lr, datefmt="%Y-%m-%d") == "2021-11-20"


def test_formatter_format_exception(error: Exception) -> None:
    lr = LogRecord(name="simple", level="info", msg="simple message", exc_info=error)
    formatter = Formatter()
    s = formatter.format_exception(lr.exc_info)
    assert s.startswith("Traceback") and s.endswith(error.__class__.__name__)


def test_formatter_format_simple() -> None:
    lr = LogRecord(
        name="simple",
        level="info",
        msg="message",
    )
    formatter = Formatter()
    assert formatter.format(lr) == "message"


def test_formatter_format_with_time() -> None:
    create_time = 1637400026
    lr = LogRecord(name="simple", level="info", msg="message", create_time=create_time)
    formatter = Formatter(fmt="[%(asctime)s] %(message)s")
    assert formatter.format(lr) == "[2021-11-20 09:20:26] message"


def test_formatter_format_with_exception(error: Exception) -> None:
    create_time = 1637400026
    lr = LogRecord(
        name="simple",
        level="info",
        msg="message",
        create_time=create_time,
        exc_info=error,
    )
    formatter = Formatter(fmt="[%(asctime)s] %(message)s")
    s = formatter.format(lr)
    assert s.startswith("[2021-11-20 09:20:26] message") and s.endswith(
        error.__class__.__name__
    )


def test_formatter_format_with_custom_time() -> None:
    create_time = 1637400026
    lr = LogRecord(name="simple", level="info", msg="message", create_time=create_time)
    formatter = Formatter(fmt="[%(asctime)s] %(message)s", datefmt="%Y-%m-%d")
    assert formatter.format(lr) == "[2021-11-20] message"


def test_formatter_format_cache_exc_text(
    mocker: MockerFixture, error: Exception
) -> None:
    lr = LogRecord(name="simple", level="info", msg="message", exc_info=error)
    formatter = Formatter()
    formatter.format(lr)
    prev_exc_text = lr.exc_text
    assert prev_exc_text

    def not_impl(*args: Any) -> None:
        raise NotImplementedError()

    mocker.patch("logger.formatter.Formatter.format_exception", not_impl)
    formatter.format(lr)
    assert prev_exc_text == lr.exc_text


def test_formatter_format_with_args() -> None:
    lr = LogRecord(name="simple", level="info", msg="%s", args=("message"))
    formatter = Formatter(fmt="%(message)s", datefmt="%Y-%m-%d")
    assert formatter.format(lr) == "message"

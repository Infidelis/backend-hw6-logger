import io
import sys
import time
from tempfile import NamedTemporaryFile

import pytest
from pytest_mock import MockerFixture

from logger import LogLevel, LogRecord
from logger.formatter import Formatter
from logger.handlers import FileHandler, Handler, StreamHandler


@pytest.fixture
def handler() -> Handler:
    return Handler(level=LogLevel.NOTSET)


@pytest.fixture
def mock_handler(mocker: MockerFixture, handler: Handler) -> Handler:
    mocker.patch(
        "logger.handlers.Handler.emit",
        lambda x, y: x,
    )
    return handler


@pytest.fixture
def formatter() -> Formatter:
    return Formatter()


def test_handler_set_level(handler: Handler) -> None:
    new_level = LogLevel.INFO
    handler.level = new_level
    assert handler.level == new_level


def test_handler_handle_record(
    mock_handler: Handler, simple_log_record: LogRecord
) -> None:
    assert mock_handler.handle(simple_log_record)


def test_handler_not_handle_record_with_level(
    mock_handler: Handler, simple_log_record: LogRecord
) -> None:
    new_level = LogLevel.CRITICAL
    mock_handler.level = new_level
    assert not mock_handler.handle(simple_log_record)


def test_handler_set_formatter(
    handler: Handler, simple_log_record: LogRecord, formatter: Formatter
) -> None:
    handler.formatter = formatter
    assert handler.format(simple_log_record) == simple_log_record.msg


def test_stream_handler_handle(
    simple_log_record: LogRecord, formatter: Formatter
) -> None:
    text_io = io.StringIO()
    stream_handler = StreamHandler(text_io)
    stream_handler.formatter = formatter
    stream_handler.handle(simple_log_record)
    assert text_io.getvalue() == f"{simple_log_record.msg}\n"


def test_stream_handler_default_stream(
    simple_log_record: LogRecord, formatter: Formatter
) -> None:
    stream_handler = StreamHandler()
    assert stream_handler.stream == sys.stdout


def test_stream_handler_null_stream(
    simple_log_record: LogRecord, formatter: Formatter
) -> None:
    stream_handler = StreamHandler(default_stream=None)
    stream_handler.formatter = formatter
    stream_handler.handle(simple_log_record)


def test_stream_handler_change_stream(
    simple_log_record: LogRecord, formatter: Formatter
) -> None:
    text_io1 = io.StringIO()
    text_io2 = io.StringIO()
    stream_handler = StreamHandler(text_io1)
    stream_handler.formatter = formatter

    stream_handler.handle(simple_log_record)

    stream_handler.stream = text_io2
    stream_handler.handle(simple_log_record)
    assert text_io1.getvalue() == text_io2.getvalue()


def test_file_handler_simple_write(
    simple_log_record: LogRecord, formatter: Formatter
) -> None:
    file = NamedTemporaryFile(mode="r+")
    handler = FileHandler(filepath=file.name)
    handler.formatter = formatter
    handler.emit(simple_log_record)
    handler.emit(simple_log_record)

    file.seek(0)
    line = file.read()

    handler.close()
    handler.close()
    file.close()
    assert line == 2 * (simple_log_record.msg + "\n")

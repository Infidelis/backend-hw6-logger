from io import StringIO

import pytest

from logger import Logger
from logger.formatter import Formatter
from logger.handlers import StreamHandler
from logger.level import LogLevel


@pytest.fixture()
def stream_handler() -> StreamHandler:
    text_io = StringIO()
    handler = StreamHandler(text_io, level="INFO", strict_level_filter=False)
    handler.formatter = Formatter()
    return handler


@pytest.fixture
def logger(stream_handler: StreamHandler) -> Logger:
    logger_ = Logger("simple logger")
    logger_.add_handler(stream_handler)
    return logger_


def test_logger_name() -> None:
    logger = Logger("simple logger")
    assert "simple logger" == logger.name


def test_logger_handlers_add(logger: Logger, stream_handler: StreamHandler) -> None:
    logger.add_handler(stream_handler)
    logger.add_handler(stream_handler)
    assert len(logger.handlers) == 1


def test_logger_handlers_remove(logger: Logger, stream_handler: StreamHandler) -> None:
    logger.add_handler(stream_handler)
    logger.remove_handler(stream_handler)
    logger.remove_handler(stream_handler)
    assert len(logger.handlers) == 0


def test_logger_handlers_level_set(
    logger: Logger, stream_handler: StreamHandler
) -> None:
    level = LogLevel.WARNING
    logger.level = level
    assert logger.handlers[0].level == level and logger.level == level


def test_logger_handlers_log_tuple(
    logger: Logger, stream_handler: StreamHandler
) -> None:
    logger.log("INFO", "%s %s", "hello", "world")
    assert stream_handler.stream.getvalue() == "hello world\n"  # type: ignore


def test_logger_handlers_log_dict(
    logger: Logger, stream_handler: StreamHandler
) -> None:
    logger.log("INFO", "%(hello)s %(world)s", hello="hello", world="world")
    assert stream_handler.stream.getvalue() == "hello world\n"  # type: ignore


def test_logger_handlers_log_wrong_call(
    logger: Logger, stream_handler: StreamHandler
) -> None:
    with pytest.raises(ValueError):
        logger.log("INFO", "%(hello)s %(world)s", "hello", world="world")


def test_logger_builder(stream_handler: StreamHandler) -> None:
    logger = Logger.build(
        logger_name="simple logger", handlers=[stream_handler], logger_level="Info"
    )
    logger.info("%s %s", "hello", "world")
    assert stream_handler.stream.getvalue() == "hello world\n"  # type: ignore

import tempfile
from io import StringIO
from pathlib import Path

from logger import LogRecord
from logger.base.file import create_file_logger
from logger.base.stream import create_stream_logger
from logger.base.console import create_console_logger
from logger.base.null import create_null_logger


def test_create_stream_logger() -> None:
    text_io = StringIO()
    logger = create_stream_logger(
        name="one stream logger",
        level="Info",
        stream=text_io,
        log_format="%(levelname)s\t%(function)s\t\t%(message)s",
    )
    logger.info("%s %s", "hello", "world")
    assert text_io.getvalue() == "INFO\ttest_create_stream_logger\t\thello world\n"


def test_create_console_logger() -> None:
    text_io_info = StringIO()
    text_io_error = StringIO()
    logger = create_console_logger(
        name="console logger",
        log_format="%(levelname)s %(message)s",
        info_stream=text_io_info,
        error_stream=text_io_error,
    )
    logger.info("%s %s", "hello", "world")
    logger.warning("%s %s", "hello", "world")
    logger.warning("%s %s", "hello", "world")

    assert text_io_info.getvalue() == "INFO hello world\n"
    assert text_io_error.getvalue() == 2 * "WARNING hello world\n"


def test_create_file_logger() -> None:
    with tempfile.TemporaryDirectory() as tmp_dir:
        file_prefix = f"{tmp_dir}/"
        logger = create_file_logger(
            name="File logger",
            file_prefix=file_prefix,
            log_format="%(levelname)s %(message)s",
        )
        logger.info("%s %s", "hello", "world")

        logger.warning("%s %s", "hello", "world")
        logger.warning("%s %s", "hello", "world")

        logger.debug("%s %s", "hello", "world")
        logger.debug("%s %s", "hello", "world")
        logger.debug("%s %s", "hello", "world")

        assert Path(f"{file_prefix}.info.log").read_text() == "INFO hello world\n"
        assert (
            Path(f"{file_prefix}.error.log").read_text() == 2 * "WARNING hello world\n"
        )
        assert Path(f"{file_prefix}.debug.log").read_text() == 3 * "DEBUG hello world\n"


def test_null_logger(simple_log_record: LogRecord) -> None:
    logger = create_null_logger()
    logger.info("how i can test it?")

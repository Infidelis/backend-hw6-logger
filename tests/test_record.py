import time
from pathlib import Path

from logger import LogRecord


def test_simple_log_record() -> None:
    record = LogRecord(
        name="simple", level="info", pathname=Path("."), msg="simple message"
    )

    assert record.get_message() == "simple message"


def test_log_record_with_tuple_args() -> None:
    msg = "simple message: %s, %s"
    args = ("Hello", "world")

    record = LogRecord(
        name="simple", level="info", pathname=Path("."), msg=msg, args=args
    )

    assert record.get_message() == "simple message: Hello, world"


def test_log_record_with_dict_args() -> None:
    msg = "simple message: %(hello)s, %(world)s"
    args = dict(hello="Hello", world="world")

    record = LogRecord(
        name="simple", level="info", pathname=Path("."), msg=msg, args=args
    )

    assert record.get_message() == "simple message: Hello, world"


def test_log_record_created_time() -> None:
    create_time = time.time()
    record = LogRecord(
        name="simple",
        level="info",
        pathname=Path("."),
        msg="simple message",
        create_time=create_time,
    )

    assert record.created == create_time


def test_log_record_exc_info_with_ex() -> None:
    error = ValueError()
    record = LogRecord(
        name="simple",
        level="info",
        pathname=Path("."),
        msg="simple message",
        exc_info=error,
    )
    assert record.exc_info == (type(error), error, error.__traceback__)


def test_log_record_exc_info_with_no_ex() -> None:
    record = LogRecord(
        name="simple",
        level="info",
        pathname=Path("."),
        msg="simple message",
        exc_info=1,
    )
    assert record.exc_info == (None, None, None)


def test_log_record_exc_info_with_ex_tuple() -> None:
    error = ValueError()
    ex_tuple = (type(error), error, error.__traceback__)
    record = LogRecord(
        name="simple",
        level="info",
        pathname=Path("."),
        msg="simple message",
        exc_info=ex_tuple,
    )
    assert record.exc_info == ex_tuple

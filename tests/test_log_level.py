import pytest

from logger.exceptions import UnknownLoggerLevel
from logger.level import LogLevel, validate_level


def test_log_level_creation_from_code() -> None:
    assert LogLevel.CRITICAL == LogLevel.from_code(LogLevel.CRITICAL.value)


def test_log_level_creation_from_code_exception() -> None:
    with pytest.raises(UnknownLoggerLevel):
        LogLevel.from_code(1337)


def test_log_level_creation_from_name() -> None:
    assert LogLevel.CRITICAL == LogLevel.from_name(LogLevel.CRITICAL.name)


def test_log_level_creation_from_name_exception() -> None:
    with pytest.raises(UnknownLoggerLevel):
        LogLevel.from_name("1337")


def test_log_level_validate_log_level() -> None:
    assert LogLevel.INFO == validate_level(LogLevel.INFO)


def test_log_level_validate_str_level_name() -> None:
    assert LogLevel.INFO == validate_level(LogLevel.INFO.name)


def test_log_level_validate_str_level_code() -> None:
    assert LogLevel.INFO == validate_level(str(LogLevel.INFO.value))


def test_log_level_validate_int_level_code() -> None:
    assert LogLevel.INFO == validate_level(LogLevel.INFO.value)


def test_log_level_validate_exception() -> None:
    with pytest.raises(UnknownLoggerLevel):
        validate_level("strange level")

import pytest
from logger import LogRecord


@pytest.fixture()
def simple_log_record() -> LogRecord:
    return LogRecord(
        name="simple",
        level="info",
        msg="simple message",
    )

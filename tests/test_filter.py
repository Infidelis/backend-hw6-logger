import os

import pytest

from logger import LogRecord
from logger.filter import Filter, Filterer, LevelFilter, NameLevelFilter, EnvFilter


@pytest.fixture()
def name_level_filter(simple_log_record: LogRecord) -> Filter:
    return NameLevelFilter("simple filter")


@pytest.fixture()
def filterer() -> Filterer:
    return Filterer()


def test_level_filter_log_record_true(simple_log_record: LogRecord) -> None:
    filter = LevelFilter(simple_log_record.name)
    assert filter.filter(simple_log_record)


def test_name_level_filter_log_record_true(simple_log_record: LogRecord) -> None:
    filter = NameLevelFilter(simple_log_record.name)
    assert filter.filter(simple_log_record)


def test_env_filter(simple_log_record: LogRecord) -> None:
    os.environ["LOG_LEVEL"] = "error"
    filter = EnvFilter()
    assert not filter.filter(simple_log_record)


def test_name_level_filter_log_record_false(simple_log_record: LogRecord) -> None:
    filter = NameLevelFilter(simple_log_record.name + "1")
    assert not filter.filter(simple_log_record)


def test_filterer_object_add_filter(
    filterer: Filterer, name_level_filter: Filter
) -> None:
    filterer.add_filter(name_level_filter)
    filterer.add_filter(name_level_filter)
    assert len(filterer.filters) == 1


def test_filterer_object_delete_filter(
    filterer: Filterer, name_level_filter: Filter
) -> None:
    filterer.add_filter(name_level_filter)
    filterer.remove_filter(name_level_filter)
    assert len(filterer.filters) == 0


def test_filterer_object_delete_unknown_filter(
    filterer: Filterer, name_level_filter: Filter
) -> None:
    filterer.remove_filter(name_level_filter)
    filterer.remove_filter(name_level_filter)
    assert len(filterer.filters) == 0


def test_filterer_object_ok_filter_record(
    filterer: Filterer, simple_log_record: LogRecord
) -> None:
    filterer.add_filter(NameLevelFilter(simple_log_record.name))
    assert filterer.filter(simple_log_record)


def test_filterer_object_not_ok_filter_record(
    filterer: Filterer, simple_log_record: LogRecord
) -> None:
    filterer.add_filter(NameLevelFilter(simple_log_record.name + "1"))
    assert not filterer.filter(simple_log_record)


def test_filterer_object_not_ok_level_filter_record(
    filterer: Filterer, simple_log_record: LogRecord
) -> None:
    filterer.add_filter(LevelFilter(level="WARN"))
    assert not filterer.filter(simple_log_record)


def test_filterer_clean_filters(filterer: Filterer, name_level_filter: Filter) -> None:
    filterer.add_filter(name_level_filter)
    filterer.clean_filters()
    assert len(filterer.filters) == 0


def test_level_filter_strict(filterer: Filterer, simple_log_record: LogRecord) -> None:
    string_level_filter = LevelFilter(
        level="NOTSET",
        strict_level_filter=True,
    )
    filterer.add_filter(string_level_filter)
    assert string_level_filter.level < simple_log_record.level
    assert not filterer.filter(simple_log_record)

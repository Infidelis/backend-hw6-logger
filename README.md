# Logger

## Описание

Собственный велосипед для логгирования

## Архитектура

В качестве вдохновения был изучен встроенный модуль в Python модуль
[logging](https://github.com/python/cpython/blob/3.9/Lib/logging/__init__.py)


**Спасибо [статье](https://khashtamov.com/ru/python-logging/) за описания объектов**

1. `Logger` -  Логер это рычаг за который мы дёргаем каждый раз, когда нам нужно записать информацию в лог.
2. `Handler` - Задача класса `Handler` и его потомков обрабатывать запись сообщений/логов. Т.е. `Handler` отвечает за то куда будут записаны сообщения.
3. `Formatter` -  класс, отвечающий за отображение лога. Если класс `Handler` ответственный за то куда будет происходить запись, то класс `Formatter` отвечает на вопрос как будет записано сообщение.
4. `Filter` - Задача класса фильтровать сообщения по заданной разработчиком логике (названию, уровню лога и тп)


## Начало работы

```python
pip3 install <путь к бинарнику>
```

Пример работы с логгером -- файл [example.py](./example.py) в корне репозитория


Реализации требуемых базовых логгеров находится в [logger.base](./logger/base).
Среди них:

1. [Запись в произвольный поток вывода](./logger/base/stream.py)
2. [Стандартный консольный вывод с разделением по уровням](./logger/base/console.py)
3. [Вывод в файлы с разделением по уровням](./logger/base/file.py)
4. [Черная дыра](./logger/base/null.py)


#№# Что реализовано

Все, ниже даны комментарии.

1. Реализованы уровни в [файле](./logger/level.py):
```python
CRITICAL = 50
FATAL = CRITICAL
ERROR = 40
WARNING = 30
WARN = WARNING
INFO = 20
DEBUG = 10
NOTSET = 0
```

Также при работе с логгером имеется возможность контроллировать работу с помощью переменной окружения
`LOG_LEVEL`.


Для этого нужно добавить к обработчику(`Handler`) фильтр `EnvFilter` в [файле](./logger/filter.py).
Пример можно посмотреть в файле [example.py](./example.py) в корне репозитория.

**P.S. Можно сделать это по-дефолту, но я осознанно выделил отдельную компоненту, дабы показать гибкость фильтрации**

2. Метаинформация

Настраивается строка формата из полей ниже.
Также можно прокидывать свои постоянные поля (пример есть в [тестах](tests/test_formatter.py)).
Посмотреть на настройку можно в файле [example.py](./example.py) в корне репозитория.


```
%(name)s            Name of the logger (logging channel)
%(levelno)s         Numeric logging level for the message (DEBUG, INFO,
                    WARNING, ERROR, CRITICAL)
%(levelname)s       Text logging level for the message ("DEBUG", "INFO",
                    "WARNING", "ERROR", "CRITICAL")
%(pathname)s        Full pathname of the source file where the logging
                    call was issued (if available)
%(filename)s        Filename portion of pathname
%(function)s        Function name (name portion of function)
%(name)s            Logger name
%(lineno)d          Source line number where the logging call was issued
                    (if available)
%(created)f         Time when the LogRecord was created (time.time()
                    return value)
%(asctime)s         Textual time when the LogRecord was created
%(message)s         The result of record.getMessage(), computed just as
                    the record is emitted
```

3. В каждому логгеру можно привязать сколь угодно много обработчиков - Множественные стоки
4. Ленивое журналирование реализвано так, как возможно на языке Python - отдельная подача аргументов, которые будут
   подставлены в строку. Можно пойти в сторону оборачивания долгих методов и вызова только при подстановке. Но это
   ли путь `python`?
5. Для подзадания "Строитель" реализован шаблонный метод `build` в классе `Logger`

## Разработка

В проекте используется:

1. Линтер для статического типизирования - [MyPy](https://mypy.readthedocs.io/en/stable/)
2. Линтер для стиля - [black](https://github.com/psf/black)
3. Сборка и установка - [poetry](https://python-poetry.org/)
4. Измерение покрытия тестами - [pytest-cov](https://pytest-cov.readthedocs.io/en/latest/)



Чтобы установить зависимости и развенуть окружение
(нужны установленные `python3.9` и `poetry`):

```bash
make install
```


Запуск тестов (также считает покрытие):

```bash
make test
```


Проверка линтерами (`mypy` - типизация, `black` - стиль):

```bash
make lint
```


Форматирование кода линтером `black`:

```bash
make format
```


Генерация `python` пакета:

```bash
make build
```


Очистка от cache файлов:

```bash
make clean
```

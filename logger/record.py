import sys
import time
from os import PathLike
from pathlib import Path
from types import TracebackType
from typing import Any, Iterable, Mapping, Optional, Type, Union

from .level import LogLevel, validate_level


__all__ = ["LogRecord"]


class LogRecord(object):
    def __init__(
        self,
        name: str,
        level: Union[LogLevel, str, int],
        msg: str,
        pathname: Optional[Union[Path, PathLike[str]]] = None,
        function: Optional[str] = None,
        lineno: Optional[int] = None,
        args: Optional[Union[Iterable[str], Mapping[int, Any]]] = None,
        exc_info: Optional[Any] = None,
        create_time: Optional[float] = None,
    ) -> None:

        self.level = validate_level(level)
        self.levelno = self.level.value
        self.levelname = self.level.name

        self.lineno = lineno
        self.function = function

        if not create_time:
            create_time = time.time()
        self.created = create_time

        self.name = name
        self.msg = msg
        self.args = args

        pathname = Path(pathname) if pathname else None
        self.pathname = pathname
        self.filename = pathname.name if pathname else None

        ex_info_type = Optional[
            tuple[
                Optional[Type[BaseException]],
                Optional[BaseException],
                Optional[TracebackType],
            ]
        ]
        self.exc_info: ex_info_type = None
        if exc_info:
            if isinstance(exc_info, BaseException):
                self.exc_info = (type(exc_info), exc_info, exc_info.__traceback__)
            elif isinstance(exc_info, tuple):
                self.exc_info = exc_info  # type: ignore
            else:
                self.exc_info = sys.exc_info()
        self.exc_text: Optional[str] = None  # used to cache the traceback text
        self.asctime: Optional[str] = None  # used in formatter
        self.message: Optional[str] = None

    def __repr__(self) -> str:  # pragma: no cover
        return '<LogRecord: %s, %s, %s, "%s">' % (
            self.name,
            self.level.name,
            self.pathname,
            self.msg,
        )

    def get_message(self) -> str:
        msg = str(self.msg)
        if self.args:
            msg = msg % self.args
        return msg

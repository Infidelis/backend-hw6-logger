from __future__ import annotations

from enum import IntEnum
from typing import Union

from logger.exceptions import UnknownLoggerLevel

__all__ = ["LogLevel", "validate_level"]


class LogLevel(IntEnum):
    CRITICAL = 50
    FATAL = CRITICAL
    ERROR = 40
    WARNING = 30
    WARN = WARNING
    INFO = 20
    DEBUG = 10
    NOTSET = 0

    @classmethod
    def from_code(cls, code: int) -> LogLevel:
        try:
            return cls(code)
        except ValueError:
            raise UnknownLoggerLevel(code)

    @classmethod
    def from_name(cls, level: str) -> LogLevel:
        try:
            return cls[level.upper()]
        except KeyError:
            raise UnknownLoggerLevel(level)


def validate_level(level: Union[LogLevel, str, int]) -> LogLevel:
    if isinstance(level, LogLevel):
        return level

    elif isinstance(level, str):
        try:
            return LogLevel.from_code(int(level))
        except ValueError:
            return LogLevel.from_name(level)

    else:
        assert isinstance(level, int)
        return LogLevel.from_code(level)

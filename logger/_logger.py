from __future__ import annotations

import inspect
from pathlib import Path
from typing import Any, Optional, Protocol, Union

from .handlers import Handler
from .level import LogLevel, validate_level
from .record import LogRecord

__all__ = ["ILogger", "Logger"]


class ILogger(Protocol):  # pragma: no cover
    def log(
        self,
        level: Union[int, str, LogLevel],
        msg: str,
        *args: Any,
        _frame_info: Optional[inspect.FrameInfo] = None,
        exception: Optional[BaseException] = None,
        **kwargs: Any,
    ) -> None:
        ...


class Logger(ILogger):
    def __init__(self, name: str, level: Union[int, str, LogLevel] = LogLevel.NOTSET):
        self.name = name
        self.handlers: list[Handler] = []
        self.level = validate_level(level)

    @property
    def level(self) -> LogLevel:
        return self._level

    @level.setter
    def level(self, level: Union[int, str, LogLevel]) -> None:
        self._level = validate_level(level)
        for handler in self.handlers:
            handler.level = self._level

    def add_handler(self, handler: Handler) -> None:
        if not (handler in self.handlers):
            self.handlers.append(handler)

    def remove_handler(self, handler: Handler) -> None:
        if handler in self.handlers:
            self.handlers.remove(handler)
            handler.close()

    def exception(self, e: BaseException) -> None:  # pragma: no cover
        self.log(LogLevel.ERROR, "", _frame_info=inspect.stack()[1], exception=e)

    def critical(self, msg: str, *args: Any, **kwargs: Any) -> None:  # pragma: no cover
        self.log(
            LogLevel.CRITICAL, msg, *args, _frame_info=inspect.stack()[1], **kwargs
        )

    def error(self, msg: str, *args: Any, **kwargs: Any) -> None:  # pragma: no cover
        self.log(LogLevel.ERROR, msg, *args, _frame_info=inspect.stack()[1], **kwargs)

    def warning(self, msg: str, *args: Any, **kwargs: Any) -> None:  # pragma: no cover
        self.log(LogLevel.WARNING, msg, *args, _frame_info=inspect.stack()[1], **kwargs)

    def info(self, msg: str, *args: Any, **kwargs: Any) -> None:  # pragma: no cover
        self.log(LogLevel.INFO, msg, *args, _frame_info=inspect.stack()[1], **kwargs)

    def debug(self, msg: str, *args: Any, **kwargs: Any) -> None:  # pragma: no cover
        self.log(LogLevel.DEBUG, msg, *args, _frame_info=inspect.stack()[1], **kwargs)

    def log(
        self,
        level: Union[int, str, LogLevel],
        msg: str,
        *args: Any,
        _frame_info: Optional[inspect.FrameInfo] = None,
        exception: Optional[BaseException] = None,
        **kwargs: Any,
    ) -> None:
        if args and kwargs:
            raise ValueError("You can't pass args and kwargs")

        level = validate_level(level)

        if not _frame_info:
            frame = inspect.stack()[1]
        else:
            frame = _frame_info
        function_name = frame.function
        lineno = frame.frame.f_lineno
        module = inspect.getmodule(frame[0])
        pathname = Path(module.__file__) if module else None

        record = LogRecord(
            level=level,
            name=self.name,
            msg=msg,
            args=args if args else kwargs,
            pathname=pathname,
            function=function_name,
            lineno=lineno,
            exc_info=(level > LogLevel.WARN) if not exception else exception,
        )
        for f in self.handlers:
            f.handle(record)

    @classmethod
    def build(
        cls,
        logger_name: str,
        handlers: list[Handler],
        logger_level: Union[int, str, LogLevel] = LogLevel.NOTSET,
    ) -> Logger:
        logger = Logger(name=logger_name, level=logger_level)
        for handler in handlers:
            logger.add_handler(handler)
        return logger

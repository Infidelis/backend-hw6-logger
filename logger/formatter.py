import io
import re
import time
import traceback
from typing import Any, Callable, Optional

from .record import LogRecord

__all__ = ["PercentStyle", "Formatter"]


class PercentStyle(object):
    default_format = "%(message)s"
    asctime_format = "%(asctime)s"
    asctime_search = "%(asctime)"
    validation_pattern = re.compile(
        r"%\(\w+\)[#0+ -]*(\*|\d+)?(\.(\*|\d+))?[diouxefgcrsa%]", re.I
    )

    def __init__(
        self, fmt: Optional[str], *, defaults: Optional[dict[str, Any]] = None
    ):
        self._fmt = fmt or self.default_format
        self.validate()
        self._defaults = defaults

    def is_uses_time(self) -> bool:
        return self._fmt.find(self.asctime_search) >= 0

    def validate(self) -> None:
        if not self.validation_pattern.search(self._fmt):
            raise ValueError(
                "Invalid format '%s' for '%s' style"
                % (self._fmt, self.default_format[0])
            )

    def _format(self, record: LogRecord) -> str:
        if defaults := self._defaults:
            values = defaults | record.__dict__
        else:
            values = record.__dict__
        return self._fmt % values

    def format(self, record: LogRecord) -> str:
        try:
            return self._format(record)
        except KeyError as e:
            raise ValueError("Formatting field not found in record: %s" % e)


class Formatter(object):
    """
    %(name)s            Name of the logger (logging channel)
    %(levelno)s         Numeric logging level for the message (DEBUG, INFO,
                        WARNING, ERROR, CRITICAL)
    %(levelname)s       Text logging level for the message ("DEBUG", "INFO",
                        "WARNING", "ERROR", "CRITICAL")
    %(pathname)s        Full pathname of the source file where the logging
                        call was issued (if available)
    %(filename)s        Filename portion of pathname
    %(function)s        Function (name portion of filename)
    %(lineno)d          Source line number where the logging call was issued
                        (if available)
    %(created)f         Time when the LogRecord was created (time.time()
                        return value)
    %(asctime)s         Textual time when the LogRecord was created
    %(message)s         The result of record.getMessage(), computed just as
                        the record is emitted

    """

    @property
    def converter(self) -> Callable[[float], time.struct_time]:
        return time.gmtime

    default_time_format: str = "%Y-%m-%d %H:%M:%S"

    def __init__(
        self,
        fmt: Optional[str] = None,
        datefmt: Optional[str] = None,
        *,
        defaults: Optional[dict[str, Any]] = None,
    ) -> None:
        self._style = PercentStyle(fmt, defaults=defaults)
        self._fmt = self._style._fmt
        self.datefmt = datefmt

    def format_time(self, record: LogRecord, datefmt: Optional[str] = None) -> str:
        ct = self.converter(record.created)
        if datefmt:
            s = time.strftime(datefmt, ct)
        else:
            s = time.strftime(self.default_time_format, ct)
        return s

    def format_exception(self, ei: Any) -> str:
        sio = io.StringIO()
        tb = ei[2]
        traceback.print_exception(ei[0], ei[1], tb, None, sio)
        s = sio.getvalue()
        sio.close()
        if s[-1:] == "\n":  # pragma: no cover
            s = s[:-1]
        return s

    def format_message(self, record: LogRecord) -> str:
        return self._style.format(record)

    def format(self, record: LogRecord) -> str:
        record.message = record.get_message()
        if self._style.is_uses_time():
            record.asctime = self.format_time(record, self.datefmt)
        s = self.format_message(record)
        if record.exc_info:
            if not record.exc_text:
                record.exc_text = self.format_exception(record.exc_info)
        if record.exc_text:
            if s[-1:] != "\n":  # pragma: no cover
                s = s + "\n"
            s = s + record.exc_text
        return s

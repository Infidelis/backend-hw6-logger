from typing import Union

__all__ = ["UnknownLoggerLevel"]


class UnknownLoggerLevel(Exception):
    def __init__(self, level: Union[int, str]):
        super().__init__(f"Unknown level name/code with value: {level}")

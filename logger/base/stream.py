from typing import Any, IO, Optional, Union

from logger import Logger, LogLevel
from logger.formatter import Formatter
from logger.handlers import StreamHandler


def create_stream_logger(
    name: str,
    level: Union[int, str, LogLevel] = LogLevel.NOTSET,
    stream: Optional[IO[Any]] = None,
    log_format: Optional[str] = None,
    datefmt: Optional[str] = None,
    defaults: Optional[dict[str, Any]] = None,
) -> Logger:
    handler = StreamHandler(
        stream=stream,
        level=level,
    )
    handler.formatter = Formatter(fmt=log_format, datefmt=datefmt, defaults=defaults)

    logger = Logger.build(logger_name=name, handlers=[handler], logger_level=level)
    return logger

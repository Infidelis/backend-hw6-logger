from logger import Logger
from logger.formatter import Formatter
from logger.handlers import NullHandler


def create_null_logger() -> Logger:
    null_handler = NullHandler()
    null_handler.formatter = Formatter()

    logger = Logger.build(logger_name="null logger", handlers=[null_handler])
    return logger

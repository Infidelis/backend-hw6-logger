import sys
from typing import Any, IO, Optional

from logger import Logger, LogLevel
from logger.formatter import Formatter
from logger.handlers import StreamHandler


def create_console_logger(
    name: str,
    log_format: Optional[str] = "[%(asctime)s] %(message)s",
    datefmt: Optional[str] = None,
    defaults: Optional[dict[str, Any]] = None,
    info_stream: IO[Any] = sys.stdout,
    error_stream: IO[Any] = sys.stderr,
) -> Logger:
    # записи уровня info выводятся в стандартный поток вывода
    info_handler = StreamHandler(
        stream=info_stream, level="Info", strict_level_filter=True
    )
    # записи уровня выше info — в стандартный поток ошибок
    error_handler = StreamHandler(
        stream=error_stream,
        level=LogLevel.WARNING,
    )

    formatter = Formatter(fmt=log_format, datefmt=datefmt, defaults=defaults)
    info_handler.formatter = formatter
    error_handler.formatter = formatter

    logger = Logger.build(logger_name=name, handlers=[info_handler, error_handler])
    return logger

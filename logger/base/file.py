from __future__ import annotations

from os import PathLike
from typing import Any, Optional

from logger import Logger, LogRecord
from logger.filter import LevelFilter
from logger.formatter import Formatter
from logger.handlers import FileHandler


def create_file_logger(
    name: str,
    file_prefix: PathLike[str] | str,
    log_format: Optional[str] = "[%(asctime)s] %(message)s",
    datefmt: Optional[str] = None,
    defaults: Optional[dict[str, Any]] = None,
) -> Logger:
    debug_handler = FileHandler(
        filepath=f"{file_prefix}.debug.log", strict_level_filter=False
    )
    debug_handler.clean_filters()

    class DebugFilter(LevelFilter):
        def filter(self, record: LogRecord) -> bool:
            return record.level <= self.level

    debug_handler.add_filter(DebugFilter(name="debug filter", level="debug"))

    info_handler = FileHandler(
        filepath=f"{file_prefix}.info.log", level="info", strict_level_filter=True
    )

    error_handler = FileHandler(
        filepath=f"{file_prefix}.error.log", level="warning", strict_level_filter=False
    )

    formatter = Formatter(fmt=log_format, datefmt=datefmt, defaults=defaults)
    error_handler.formatter = formatter
    info_handler.formatter = formatter
    debug_handler.formatter = formatter

    logger = Logger.build(
        logger_name=name, handlers=[debug_handler, info_handler, error_handler]
    )
    return logger

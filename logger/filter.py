from __future__ import annotations

import os
from typing import Any, Union

from .level import LogLevel, validate_level
from .record import LogRecord


__all__ = ["Filter", "EnvFilter", "LevelFilter", "NameLevelFilter", "Filterer"]


class Filter:
    def __init__(
        self,
        name: str = "",
        level: Union[str, int, LogLevel] = LogLevel.NOTSET,
        **kwargs: Any,
    ):
        self.name = name
        self.level = validate_level(level)

    def filter(self, record: LogRecord) -> bool:  # pragma: no cover
        raise NotImplementedError()


class EnvFilter(Filter):
    def __init__(
        self,
        name: str = "env filter",
        **kwargs: Any,
    ):
        Filter.__init__(self, name=name)
        self.env_log_level = validate_level(os.getenv("LOG_LEVEL", "NOTSET"))

    def filter(self, record: LogRecord) -> bool:  # pragma: no cover
        return self.env_log_level <= record.level


class LevelFilter(Filter):
    def __init__(
        self,
        name: str = "",
        level: Union[str, int, LogLevel] = LogLevel.NOTSET,
        strict_level_filter: bool = False,
        **kwargs: Any,
    ):
        Filter.__init__(self, name=name, level=level)
        self.strict_level_filter = strict_level_filter

    def filter(self, record: LogRecord) -> bool:
        if not self.strict_level_filter:
            return self.level <= record.level
        else:
            return self.level == record.level


class NameLevelFilter(LevelFilter):
    def filter(self, record: LogRecord) -> bool:
        is_filtered = super(NameLevelFilter, self).filter(record)
        return is_filtered and record.name == self.name


class Filterer(object):
    def __init__(self) -> None:
        self.filters: list[Filter] = []

    def add_filter(self, filter_: Filter) -> None:
        if not (filter_ in self.filters):
            self.filters.append(filter_)

    def remove_filter(self, filter_: Filter) -> None:
        if filter_ in self.filters:
            self.filters.remove(filter_)

    def clean_filters(self) -> None:
        self.filters = []

    def filter(self, record: LogRecord) -> bool:
        res = True
        for f in self.filters:
            is_valid = f.filter(record)

            if not is_valid:
                res = False
                break
        return res

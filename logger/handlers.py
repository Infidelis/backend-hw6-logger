import sys
from pathlib import Path
from typing import Any, IO, Optional, Union

from .filter import Filterer, LevelFilter
from .formatter import Formatter
from .level import LogLevel, validate_level
from .record import LogRecord

__all__ = ["Handler", "StreamHandler", "FileHandler", "NullHandler"]


class Handler(Filterer):
    """
    Handler instances dispatch logging events to specific destinations.
    The base handler class. Acts as a placeholder which defines the Handler
    interface. Handlers can optionally use Formatter instances to format
    records as desired. By default, no formatter is specified; in this case,
    the 'raw' message as determined by record.message is logged.
    """

    def __init__(
        self,
        level: Union[int, str, LogLevel] = LogLevel.NOTSET,
        strict_level_filter: bool = False,
    ):
        Filterer.__init__(self)
        self.name = None
        self._level_filter = None
        self._formatter: Optional[Formatter] = None
        self._closed = False
        self._strict_level_filter = strict_level_filter

        self.level = validate_level(level)

    @property
    def level(self) -> LogLevel:
        return self._level

    @level.setter
    def level(self, level: Union[int, str, LogLevel]) -> None:
        self._level = validate_level(level)
        if self._level_filter:
            self.remove_filter(self._level_filter)

        self._level_filter = LevelFilter(
            name="level_filter",
            level=self._level,
            strict_level_filter=self._strict_level_filter,
        )
        self.add_filter(self._level_filter)

    def format(self, record: LogRecord) -> str:
        return self.formatter.format(record)

    def emit(self, record: LogRecord) -> None:  # pragma: no cover
        raise NotImplementedError("emit must be implemented " "by Handler subclasses")

    def handle(self, record: LogRecord) -> bool:
        rv = self.filter(record)
        if rv:
            self.emit(record)
        return rv

    @property
    def formatter(self) -> Formatter:
        assert self._formatter is not None, "Formatter is None"
        return self._formatter

    @formatter.setter
    def formatter(self, fmt: Formatter) -> None:
        self._formatter = fmt

    def flush(self) -> None:  # pragma: no cover
        """
        Ensure all logging output has been flushed.
        This version does nothing and is intended to be implemented by
        subclasses.
        """
        pass

    def close(self) -> None:  # pragma: no cover
        self._closed = True

    def __repr__(self) -> str:  # pragma: no cover
        return "<%s (%s)>" % (self.__class__.__name__, self.level.name)


class NullHandler(Handler):
    def emit(self, record: LogRecord) -> None:  # pragma: no cover
        pass


class StreamHandler(Handler):
    """
    A handler class which writes logging records, appropriately formatted,
    to a stream. Note that this class does not close the stream, as
    sys.stdout or sys.stderr may be used.
    """

    terminator = "\n"

    def __init__(
        self,
        stream: Optional[IO[Any]] = None,
        default_stream: Optional[IO[Any]] = sys.stdout,
        level: Union[int, str, LogLevel] = LogLevel.NOTSET,
        strict_level_filter: bool = False,
    ):
        Handler.__init__(self, level=level, strict_level_filter=strict_level_filter)
        if stream is None:
            self._stream: Optional[IO[Any]] = default_stream
        else:
            self._stream = stream

    @property
    def stream(self) -> Optional[IO[Any]]:
        return self._stream

    @stream.setter
    def stream(self, stream: IO[Any]) -> None:
        self.flush()
        self._stream = stream

    def flush(self) -> None:
        if self.stream:
            self.stream.flush()

    def emit(self, record: LogRecord) -> None:
        msg = self.format(record)
        stream = self.stream
        if stream:
            stream.write(msg + self.terminator)
        self.flush()

    def __repr__(self) -> str:  # pragma: no cover
        name = getattr(self.stream, "name", "")
        name = str(name)
        if name:
            name += " "
        return "<%s %s(%s)>" % (self.__class__.__name__, name, self.level.name)


class FileHandler(StreamHandler):
    """
    A handler class which writes formatted logging records to disk files.
    """

    def __init__(
        self,
        filepath: Union[str, Path],
        mode: str = "a",
        encoding: Optional[str] = None,
        level: Union[int, str, LogLevel] = LogLevel.NOTSET,
        strict_level_filter: bool = False,
    ):
        super().__init__(
            default_stream=None, level=level, strict_level_filter=strict_level_filter
        )

        filepath = Path(filepath)
        self.filepath: Path = filepath
        self.mode = mode
        self.encoding = encoding
        self._builtin_open = open

    def close(self) -> None:
        try:
            if self.stream:
                try:
                    self.flush()
                finally:
                    stream = self.stream
                    self.stream = None
                    stream.close()
        finally:
            StreamHandler.close(self)

    def _open(self) -> IO[Any]:
        open_func = self._builtin_open
        return open_func(self.filepath, self.mode, encoding=self.encoding)

    def emit(self, record: LogRecord) -> None:
        if self.stream is None:
            self.stream = self._open()
        StreamHandler.emit(self, record)

    def __repr__(self) -> str:  # pragma: no cover
        return "<%s %s (%s)>" % (
            self.__class__.__name__,
            self.filepath,
            self.level.name,
        )

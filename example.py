from logger.base.console import create_console_logger
from logger.filter import EnvFilter


def main() -> None:
    logger = create_console_logger(
        name="console logger",
        log_format="[%(asctime)s]\t%(pathname)s\t%(levelname)s\t%(function)s:%(lineno)s\t\t%(message)s",
    )
    logger.handlers[0].add_filter(EnvFilter())

    logger.info("%s %s", "hello", "world")
    logger.warning("%(hello)s %(world)s", hello="hello", world="world")
    logger.critical("%s %s", "hello", "world")

    try:
        logger.error("%(hello)s %s!", "world", hello="hello")
    except ValueError as e:
        logger.exception(e)


if __name__ == "__main__":
    main()
